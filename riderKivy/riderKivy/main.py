﻿#Written by Jarred Benham and Caitlyn Zhou
#Date: 15/8/15

__version__ = "1.0"
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
import md5

Builder.load_file('riderKivy.kv')

username = "Jarred Benham"
driverstatus = "Driver" #1 = Driver, 2 = 
phone = "95835655"
email = "a@outlook.com"
maxpassengers = 2
apiURL = "https://bad595c3.ngrok.io"

class MapScreen(Screen):
    pass
class SignInScreen(Screen):
    def do_login(self, loginText, passwordText):
        print loginText
        passwordhash = md5.new()
        passwordhash.update(passwordText)
        passwordhash.digest()
        #Send this to the server
        #If the login is successful
        #Do a get info call here
        self.manager.transition.direction = "left"
        self.manager.current = 'map'
        #If the login isn't succesful
        #Draw a label that says not succesful

    def resetForm(self):
        self.ids['login'].text = "Username"
        self.ids['password'].text = ""

class SignUpScreen(Screen):
    #Get the info from the sign up screen and send it to the server
    def sign_up(self, textMobile, textFirstName, textLastName, textEmail, textDriver, textMaxPassengers):
        #Send this information to the server
        print textMobile + textFirstName + textLastName + textEmail + textDriver + textMaxPassengers
        self.manager.transition.direction = "up"
        self.manager.current = 'signupdirections'
class SignUpDirectionsScreen(Screen):
    def send_signup(self):
        pass
    #Send the address and calendar variables to the server
class SettingsScreen(Screen):
    #Get this information from the JSON repository
    #Display all the information about the user
    #Name
    #Driver/Passenger/Both
    #Phone
    #Email
    #Max number of passengers
    global username
    global phone
    global driverstatus
    global email
    global maxpassengers
    
    drivername = username
    phonenum = phone
    status = driverstatus
    eaddress = email
    numpassengers = maxpassengers

    #Next step is making these fields editable


class SettingsDirectionsScreen(Screen):
    #Address
    #Timetable
    #Destination
    pass

screenManager = ScreenManager()
screenManager.add_widget(SignInScreen(name="login"))
screenManager.add_widget(MapScreen(name='map'))
screenManager.add_widget(SignUpScreen(name="signup"))
screenManager.add_widget(SignUpDirectionsScreen(name="signupdirections"))
screenManager.add_widget(SettingsScreen(name="settings"))
screenManager.add_widget(SettingsDirectionsScreen(name="settingsdirections"))

class riderApp(App):

    def build(self):
        return screenManager

if __name__ == '__main__':
    riderApp().run()